//para usar cout y cin
#include "iostream"
//para usar tipos de datos string
#include "string"
//para no cerrar el programa
#include "conio.h"
//para concatenacion usando trasmicion de string
#include "sstream"

using namespace std;

string tabla = "Resultado : \n|cedula\t|a�os\t|gratificacion\t|\n";

float getGratificacion(int annios)
{
	int res = 0;
	
	if ( annios >= 10 || annios <20 )
		res = 80000;
	if ( annios >= 20 || annios <=30 )
		res = 100000;
	if ( annios > 30 )
		res = 150000;
	
	return res;	
}

main()
{
	float cedula;
	int annios;
	float totalGrat = 0;
	float cantEmpl = 0;
	
	cout << "Digite Numero de Cedula  o Cero para salir \n" << endl;
	cin >> cedula;
	
	while(cedula != 0)
	{
		//pido y guardo los a�os
		cout << "Digite A�os Trabajados para la cedula " << cedula << "\n" << endl;
		cin >> annios;
		
		//proceso de datos
		cantEmpl++;
		float grat = getGratificacion(annios);
		totalGrat += grat;
		
		//concateno en la tabla de resultados
		ostringstream ss;
		ss << "|" << cedula << "\t|"<< annios << "\t|" << grat << "\t|\n";
		tabla = tabla + ss.str();
		
		cout << "Digite Numero de Cedula  o Cero para salir \n" << endl;
		cin >> cedula;
	}
	
	cout << tabla << "|Promedio :\t" << (totalGrat/cantEmpl) << endl;		 
	
	getch();
}
