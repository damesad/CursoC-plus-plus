#include "iostream"
#include "string"
#include "conio.h"
#include "sstream"

using namespace std;

int listaProductos()
{
	cout  << "\n***** LISTADO DE PRODUCTOS  *****" << "\n"
		  << "1. Gaseosa en lata: $2.000 pesos" << "\n"
	      << "2. Detergente Ariel por 5 kilos: $23.000" << "\n"
	      << "3. Salsa de Soya: $2.500 pesos" << "\n"
	      << "4. Cloro en Galon: $8.000 pesos" << "\n"
	      << "5. Mantequilla: $4.000 pesos" << "\n" << endl;      
}

float precio(int producto)
{
	int resultado=0;
	switch(producto)
	{
		case 1:
			resultado = 2000;
			break;
		case 2:
			resultado = 23000;
			break;
		case 3:
			resultado = 2500;
			break;
		case 4:
			resultado = 8000;
			break;
		case 5:
			resultado = 4000;
			break;	
		default:
			resultado = 0;
	}
	return resultado;
}


float descuento( float valor )
{
	if ( valor > 100000 && valor <= 400000)
	{
		printf("Descuento : 10% \n");
		return valor * 0.10;
	}
	if ( valor > 400000 && valor <= 600000)
	{
		printf("Descuento : 5% \n");
		return valor * 0.5;
	}
	if ( valor > 600000 && valor <= 800000)
	{
		printf("Descuento : 20% \n");
		return valor * 0.20;
	}
	if ( valor >  800000)
	{
		printf("Descuento : 25% \n");
		return valor * 0.25;
	}
	
	return valor;
}



main(){
	cout << "BIENVENIDO A LA TIENDA " << endl;
	bool salir=false;
	float valorCompra=0;
	float totalIngresos = 0;
	float cantidadClientes = 0;
	
	do{
		int menu1;
		cout << "\nESCOJA UNA OPCION" << "\n"
			<< " 1. ESCOGER PRODUCTO" << "\n"
			<< " 2. PASAR  A CAJA REGISTRADORA"<< "\n"
			<< " 3. LIMPIAR LISTA (esto eliminara productos escojidos)" << "\n"
			<< " 4. VER ESTADO TIENDA"<< "\n"
			<< " 5. SALIR "<< "\n"
			<< "Estado Compra : " << valorCompra << endl ;
		cin>> menu1;
		
		switch(menu1){
			//escojer producto
			case 1:
				int producto;
				listaProductos();
				cin >> producto;
				valorCompra += precio(producto);		
			break;	
			// facturar
			case 2:
				printf("\n ******** \n VALOR COMPRA %f \n",valorCompra);
				valorCompra = descuento(valorCompra);
				cout << "PAGAR: " << valorCompra << endl;
				totalIngresos += valorCompra;
				cantidadClientes++;
			//para a limpiar lista
			case 3:
				valorCompra = 0;
			break;
			// estado de la tiendo
			case 4:
				printf("TOTAL DE COMPRAS %f \n",totalIngresos);
				cout << "PROMEDIO : " <<  totalIngresos/cantidadClientes << endl;
			break;
			case 5:
				salir = true;
			break;
		}		
	}
	while(!salir);
		      
}
