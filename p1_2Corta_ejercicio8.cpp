#include "iostream"
#include "string"
#include "conio.h"
#include "sstream"

/*	ostringstream ss;
		ss << "|" << cedula << "\t|"<< annios << "\t|" << grat << "\t|\n";
		db = db + ss.str();
*/
using namespace std;

string strcaravaMayorPeso;
int caravanaMayorPeso;

string strvolquetaMasCarbon;
int volquetaMasCarbon;

string strcaravaMasVolquetas;
int caravanaMasVolquetas;

float totalCarbon;




float pesoVolqueta(int codVol)
{
	int num = codVol;
	float peso = 0;
	
	//numero 6 cifras mod 1000 ultimas 3
	peso = codVol%1000;
	// numero 3 cifras divido 10 un decimal
	peso = peso/10;
	
	return peso;
}



void calVolquetaMasCarbon(int cantCarbon,int codVol){
	if(volquetaMasCarbon <= cantCarbon)
		{
			ostringstream ss;
			ss << "COD:"<< codVol << " - PESO:"<< cantCarbon << "\n";
		
			if(volquetaMasCarbon < cantCarbon){
				strvolquetaMasCarbon = "";
			}
			strvolquetaMasCarbon = strvolquetaMasCarbon + ss.str();
			volquetaMasCarbon = cantCarbon;
		}
}


void calCaravanaMasVolquetas(int cantVolquetas,int codCarabana){
	if(caravanaMasVolquetas <= cantVolquetas)
		{
			ostringstream ss;
			ss << "COD:"<<codCarabana << " - CANT:"<< cantVolquetas <<"\n";
			
			if(caravanaMasVolquetas < cantVolquetas){
				strcaravaMasVolquetas = "";
			}
			
			strcaravaMasVolquetas = strcaravaMasVolquetas + ss.str();
			caravanaMasVolquetas = cantVolquetas;
		}
}

void calCaravaMayorPeso(int pesoCaravana,int codCarabana){
	
	if(caravanaMayorPeso <= pesoCaravana)
		{
			ostringstream ss;
			ss << "COD:" << codCarabana << " - PESO:"<< pesoCaravana << "\n";
			
			if (caravanaMayorPeso < pesoCaravana){
				strcaravaMayorPeso="";
			}
			strcaravaMayorPeso= strcaravaMayorPeso + ss.str();
			caravanaMayorPeso = pesoCaravana;
		}
	
}




void resgistroCaravana(int num){
	
	float cantCarbon ; 
	int numVolquetas;
	bool finRegistro=false;
	
	do{
		
		int codVol;
		float peso,cantCarbonVol;
	
		cout << "\n CODIGO DE VOLQUETA : ";
		cin>> codVol;
		
		//codigo de 6 cifras
		if(codVol > 100000 && codVol < 1000000){	
			//validar que el peso sea mayor al de la volqueta
			do{
				cout << " PESO EN VASCULA : ";
				cin>> peso;
			}	
			while(pesoVolqueta(codVol) > peso);
			
			cantCarbonVol = peso - pesoVolqueta(codVol);
			calVolquetaMasCarbon(cantCarbonVol,codVol);
			cantCarbon+= cantCarbonVol;
			numVolquetas++;
			
			cout << "Registro Exitoso - " << codVol << "- "<< peso << "-" << cantCarbonVol <<endl;
			
		}
		if(codVol == 0)
		{
			finRegistro = true;
		}
	}
	while(!finRegistro);
	
	calCaravaMayorPeso(cantCarbon,num);
	calCaravanaMasVolquetas(numVolquetas,num);
}


main()
{
	cout << "APP EL CARBONERO S.A" 
		 << "--------------------\n"
		 <<endl;
	int menu;
	do{
		cout << "\n1. Registrar Caravana\n"
		 	 << "2. Generar Informe\n"
		 	 << "3. Salir\n"
	   		 << endl;
		cin>>menu;
		switch(menu)
		{
			//registar caravana
			case 1:
				int num;
				cout << "Codigo de Caravana : " ;
				cin >> num;
				resgistroCaravana(num);
				break;
			// imprimir informe
			case 2:
			     cout << "\n-----INFORME-----\n"
				 	  << "\n* CARAVANAS MAYOR PESO\n" << strcaravaMayorPeso
			     	  << "\n* CARAVANAS MAS VOLQUETAS\n" <<strcaravaMasVolquetas
				      << "\n* VOLQUETAS MAS CARBON\n" <<strvolquetaMasCarbon
				      << "\n-----END---------\n"
				      << endl;
				break;
			// salir
			case 3:	
			     menu = 0;	 
		}
	}
	while(menu);
}
