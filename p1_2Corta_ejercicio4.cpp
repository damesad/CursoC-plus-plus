#include "iostream"
#include "string"
#include "conio.h"
#include "sstream"

using namespace std;

int sumaDigitos( int num )
{
	int s, r;

	s = 0;

	while(num>0)
	{
		r = num % 10;
		s = s + r;
		num = num / 10;
	}
	
	return s;
}

int removeUltimoDigito( int num)
{
	int mod = num % 10;
	num -= mod;
	num /=10;
	
	return num;
}

int serieNumero()
{
	int num;
	int anterior = 1;
	int suma=1;
	cout << "\n SERIE DE NUMERO " << endl;
	cout << "Digite un numero " << endl;
	cin >> num;
	
	
	for(int i=1;i<num;i++){
		
		if( i == 1){
			cout << i << ", ";	
		}
		else
		{
			int temporal = suma;
			suma = anterior + suma;
			anterior = temporal;
		}
	    cout << suma << ", ";
	}
}

//hacer que un numero tenga formato de tiempo
// si es 1 return 01 , 59 return 59
string formatoT(int num,int mayor)
{
	string resultado;
	//concatenar
	ostringstream ss;
	if(num <= mayor ){
		if ( num < 10){	
			//le concateno un cero
			ss << "0" <<num;
		}
		else
		{	
			ss << num;
		}
	}
	else
	{
		ss << "00";
	}
	//actualizo el resultado
	resultado =  ss.str();
	return resultado;
}


int relojDigital()
{
	int h,m,s;
	cout << "RELOJ DIGITAL" << endl;
	cout << "Digite un numero para las Horas" << endl;
	cin >> h;
	cout << "Digite un numero para los Minutos" << endl;
	cin >> m;
	cout << "Digite un numero para los Segundos" << endl;
	cin >> s;
	
	cout << formatoT(h,24) << ":" << formatoT(m,60) << ":" << formatoT(s,60) << endl;
}

int elevado()
{
	int x,z,num;
	num = 0;
	cout << "NUMERO ELEVADO X ^ Z " << endl;
	cout << "Digite un numero para X" << endl;
	cin >> x;
	cout << "Digite un numero para Z" << endl;
	cin >> z;
	
	for( int i = 0; i <= z ; i++)
	{
		num = z*z;
		//todo numero elvado a la cero es igual a 1
		if(z==0)
		{
			num = 1;	
		}
	}
	
	cout << x << "^" << z << " = " << num << endl; 	
}

main()
{
	int num;
	
	cout << "BIENVENIDO AL JUEGO " <<
	"\n Ingrese Un Numero" << endl;
	cin >> num;
	
	while(sumaDigitos(num)>10){
		cout << sumaDigitos(num) << endl;
		num = removeUltimoDigito(num);
	}
	
	if(num!=0)
	{
		bool salir = false;
		cout << "CONTINUEMOS INGRESA OTRO NUMERO" << endl;
		while(!salir){
			cin >> num;
			switch(num)
			{
				case 1:
					serieNumero();
					break;
				case 2:
					relojDigital();
					break;
				case 3:
					elevado();
					break;
				case 4:
				    salir = true;
					break;	
			}
		}
	}
	
	printf ("EL JUEGO A TERMINADO");
	
	getch();
}
