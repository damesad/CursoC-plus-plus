/************
 ALMACENES MACRO
 *************/
#include "iostream"
#include "string"
#include "conio.h"
#include "sstream"

using namespace std;

/***************
  FUNCIONES SIMPLES
***************/

/** retorna el digito selecionado del numero ingresado*/ 
int obtenerDigito(int numero,int largo,int digito)
{
    int modulo;
    int veces = (largo - digito)+1;
    do
    {
           modulo = numero%10;
           numero = (numero - modulo) / 10;
           veces --;
           cout << "[test - obtenerDigito] " 
                << " modulo : " << modulo
                << " numero : " << numero
                << " veces  : " << veces << endl;
    }
    while(veces);
    
    return modulo;
}

/***************
 FUNCIONES COMPLEJAS
****************/

/* Determinar Si el dia esta comprendido entre 1 - 31*/
int validar(int dia)
{
    if(dia <= 0 || dia > 31 )
    {
         dia = 0;// cero o false
    }
    return dia;    
}

/* verifica que el codigo sea de 4 digitos y el primero sea impar*/
int verificar(int codPt)
{
    //validar 4 cifras
    if(codPt > 999 && codPt <= 9999)
    {
      //obtener primer digito
      int digito = (int) codPt/1000;
      cout << "[test - verificar ] " 
           << " codPt : " << codPt 
           << " digito : " << digito << endl;
      if( (digito % 2) != 0)//digito impar
      {
          return codPt;
      }
    }
    return codPt=1;   
}

float descuentoPromo( int codPt,float precioPt , int dia)
{
    int ultimo = obtenerDigito(codPt,4,4);
    int penultimo = obtenerDigito(codPt,4,3);
    // 3567 - ultimo 7 , penultimo 6 :: (6*10)+7 = 67
    int dosUltimos = (penultimo*10) + ultimo;
    if( dosUltimos == dia)
    {
        //descuneto de 10%
        precioPt = precioPt - (precioPt*0.10);
    }
    cout << "[test- descuentoPromo] " 
         << " dia: " << dia 
         << " dosultimos : " << dosUltimos 
         << " precio Actual : " << precioPt << endl; 
    return precioPt; 
}

void leerProducto(int *codigo, float *precio)
{
     int c;
     int p;
     cout << "Digite Codigo Producto (4 cifras) : ";
     cin >> *codigo;
     cout << "Digite Precio Producto : $";
     cin >> *precio;
}

void leerProductos(int cantProductos , int dia, float *valorCompra)
{
      float valorPagar;
      while(cantProductos)//pedir N productos
      {
          //puntero codigo
         int codPt;
         int *codigo;
         codigo = &codPt;
         //puntero precio
         float precioPt;
         float *precio;
         precio = &precioPt;
         
         //verficar codigo 
         //pedir codigo hasta ingresar una valido
         do
         {
           leerProducto(codigo,precio);
           if(verificar(codPt)==1){
            cout << "ERROR - codigo de producto" << codPt << "no es valido";
           }
         } 
         
         while(verificar(codPt) == 1);
                        
         //facturar y calcular descuestos.
        
         precioPt = descuentoPromo(codPt,precioPt,dia);
         *valorCompra += precioPt;
         cantProductos--;  
         
         cout << "[test - leerProducto] " 
         << " precio : " << precioPt 
         << " dia :  " << dia 
         << " Valor a Pagar : " << *valorCompra<< endl;                
      }//fin while  
}

int cedulaGanadora(int cedula, int dia)
{
    int sumatoria;
    do
    {
           int  modulo = cedula%10;
           sumatoria += modulo;
           cedula= (cedula - modulo) / 10;
          
           cout << "[test - cedulaGanadora] "
                << " cedula : " << cedula 
                << " dia : " << dia
                << " modulo : " << modulo
                << " sumatoria  : " << sumatoria << endl;
    }
    while(cedula);

    if(sumatoria == dia)
    {
      return 1;
    }
    return 0;
}

main()
{
      bool salir = false;//controla salida de aplicacion
      float totalVenta = 0;
      int ccVenta = 0;//cedula cliente mayor venta
      float vlVenta = 0; // dinero dicho cliente gasto
      do{
         
         
           
         cout << "\n ** ALMACENES MACRO ** "<< endl;
         cout << " 1 - Apertura de Caja\n"
              << " 2 - Reporte Dia\n"
              << " 0 - Salir\n" 
              << " Opcion : ";
         int opcion;
         cin >> opcion;
      
         switch(opcion)
         {
            case 0:
                 salir = true;
                 break;
            case 1: //inicia aplicacion
                 bool next;
                 int dia;
                 int cedula;
                 //string nombre;
                // string telefono;
                 //string email;
                 //string sexo;
                 int cantProductos;
                 
                 //Validar dia ingresado        
                 cout << "\n Digite Dia : ";
                 cin >> dia;
                 next = validar(dia);
                 if(next)
                 {
                     cout << "--> DIA " << dia << " - BIENVENIDO" << endl;
                 }
                 else
                 {
                     cout << "--> EL DIA " << dia << " ES ERRONEO :(" << endl;
                 }
                     
                     
                 while(next)
                 {
                   cout << "\n** Datos de  Cliente **\n";
                   /*se comentaran datos no necesarios para test
                   cout << "Nombre : " ;
                   cin >> nombre; */
                   cout << "Numero Cedula : ";
                   cin >> cedula;
                   if(cedula !=0){
                     /*cout << "Numero Telefono : ";
                     cin >> telefono;
                     cout << "Correo / email : ";
                     cin >> email;
                     cout << "Sexo [ f - m ] : ";
                     cin >> sexo;*/
                     cout << "Cantidad de Productos : ";
                     cin >> cantProductos;
                     
                     cout << "\n** FACTURACION **\n ";
                     if( cedulaGanadora(cedula , dia))
                     {
                       cout << "*******************"
                            << "\n FELIZITACIONES !!!! \n"
                            << "SU NUMERO DE CEDULA " << cedula << "\n"
                            << "ES GANADORA PARA HOY " << dia << "\n"
                            << "DISFURTA DE 100% DE DESCUENTO EN TU COMPRA"
                            << "\n*******************\n" << endl;
                     }
                     else
                     {
                      float valorCompra;
                      leerProductos(cantProductos,dia,&valorCompra);
                      cout << "--> VALOR TOTAL A PAGAR $" << valorCompra << endl;
                      totalVenta += valorCompra;
                      // validar cliente mayor compra
                      if ( valorCompra > vlVenta)
                      {
                           vlVenta = valorCompra;
                           ccVenta = cedula;
                      }
                     }
                   }
                   else { 
                        next = cedula;
                   }
                 } //fin while
                 break;
                case 2: 
                     cout << "\n --- *****  ----"
                          << "\n\n TOTAL DEL DIA EN CAJA : $" << totalVenta
                          << "\n CLIENTE MAYOR COMPRA :"
                          << "\n  cedula : " << ccVenta 
                          << "\n  valor compra : $" << vlVenta 
                          << "\n\n --- *****  ----" << endl;            
         } //fin switch 
      }//fin do
      while(!salir);
      
}
