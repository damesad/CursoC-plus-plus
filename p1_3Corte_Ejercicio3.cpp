/************
 NUMERO ANCLADO
 *************/
#include "iostream"
#include "string"
#include "conio.h"
#include "sstream"
#include "math.h"

using namespace std;

int validar_tamano_del_numero(int numero)
{
  if(numero > 99999 )
  {
            return numero;
  }
  return 0;
}

int suma_digitos(int numero)
{
    int suma;
    while(numero !=0)
    {
      int digito = numero%10;
      suma += digito;
      numero -=digito;
      numero /=10;             
    } 
    
    return suma;   
}

int cantidad_digitos(int numero)
{
    int cantidad = 0;
    while(numero >= 1)
    {
      numero /=10;   
      cantidad++ ;         
    } 
    
    return cantidad;
}

int obtener_digito(int numero,int posDigito)
{
    int modulo = 0;
    int cantC =  cantidad_digitos(numero);
    int veces = cantC-posDigito;
    
    for(int i=0; i <= veces ; i++)
    {       
            modulo = numero%10;
            numero = (numero-modulo)/10;
    }
    
    return modulo;
}

int obtener_numero_validar(int numero,int inicio = 3,int fin = 4)
{

    float numeroValidar = 0;
    int contador=0;
    //formula  n(x)*(10^(y))+n(x+1)*(10^(y-1))+..
    //123 equivale a (1*(10^3))+(2*(10^2))+(3*(10^1))= 123
    while(fin >= inicio)//inica menor potencia osea el ultimo digito
    {
        //para obtener el digito
        int digito = obtener_digito(numero,fin);
        //(10^(y))
        float posDecimal = pow(10,contador);
        // n(x)*(10^(y))
        numeroValidar +=  digito * posDecimal;
        fin--;  
        contador++;              
    }
              
    return (int) numeroValidar;
}

void anclado(int numero,int inicio = 3,int fin = 4)
{

    //cantidad cifras al cuadraro es igual a la sumatoria de sus digitos
    // ej: 13 -> 2*2 =4 , 1+3 = 4
    cout << "NUMERO [ " << numero << "]\n";
    int validar = obtener_numero_validar(numero,inicio,fin);
    cout << "Validar " << validar << endl;
    
    int cantCifras = cantidad_digitos(validar);
    float cuadrado = pow(cantCifras,2);
    cout << "Cuadrado " << cuadrado << endl;
    
    int sumatoria = suma_digitos(validar);
    cout << "Suma " << sumatoria << endl;
    
    
    if(cuadrado == sumatoria)
    {
       cout << "este numero ES ANCLADO" << endl;
    }
    else
    {
       cout << "este numero NO ES ANCLADO" << endl;
    }
    
    cout << "\n**********\n " << endl;
}

void ajustar_numero(int numero,int inicio = 3,int fin = 4)
{
      if(suma_digitos(numero)!= 2)
      {
           numero++;                                             
      }
      anclado(numero,inicio,fin);             
}
                   

int validar_numero_primo_o_no(int numero,int inicio = 3,int fin = 4)
{
   //rango de prueba
   float raiz = sqrt(numero);
   int cantDivisores=2;
   int divisor = 2;
   while ((divisor <= raiz) && divisor<=2)
   {
      if((numero%divisor)==0)
      { 
         cantDivisores++;
      }

      /*cout << " numero : " << numero
           << " divisor : " << divisor
           << " cantidad : " << cantDivisores << endl;*/
      divisor++;
   }
   if(cantDivisores == 2 )
   { 
      return 1;
   }
   else {
       ajustar_numero(numero,inicio,fin);
   }
}

void test()
{
    string res;
    
    cout << "validar_tamano_del_numero()" << endl;
    res = (validar_tamano_del_numero(37)==0) ? "OK " : "FAIL";
    cout << res << endl;
    res = (validar_tamano_del_numero(999999)!=0) ? "OK " : "FAIL";
    cout << res << endl;
    
    cout << "validar_numero_primo_o_no()" << endl;
    res = (validar_numero_primo_o_no(37,0,0)==1) ? "OK " : "FAIL";
    cout << res << endl;
    res = (validar_numero_primo_o_no(2,0,0)==1) ? "OK " : "FAIL";
    cout << res << endl;
    res = (validar_numero_primo_o_no(4)!=1) ? "OK " : "FAIL";
    cout << res << endl;
    
    cout << "suma_digitos()" << endl;
    res = (suma_digitos(13)==4) ? "OK" : "FAIL";
    cout << res << endl;
    res = (suma_digitos(100)==1) ? "OK" : "FAIL";
    cout << res << endl;
    res = (suma_digitos(1)==1) ? "OK" : "FAIL";
    cout << res << endl;
    
    cout << "cantidad_digitos()" << endl;
    res = (cantidad_digitos(41)==2) ? "OK" : "FAIL";
    cout << res << endl;
    res = (cantidad_digitos(444)==3) ? "OK" : "FAIL";
    cout << res << endl;
    res = (cantidad_digitos(1)==1) ? "OK" : "FAIL";
    cout << res << endl;
    
    cout << "obtener_digito()" << endl;
    res = (obtener_digito(6543210,1)==6) ? "OK" : "FAIL";
    cout << res << endl;
    res = (obtener_digito(6543210,7)==0) ? "OK" : "FAIL";
    cout << res << endl;
    res = (obtener_digito(6543210,3)==4) ? "OK" : "FAIL";
    cout << res << endl;
    
    cout << "obtener_numero_validar" << endl;
    res = (obtener_numero_validar(6543210,1,3)==654) ? "OK" : "FAIL";
    cout << res << endl;
    res = (obtener_numero_validar(6543210,1,1)==6) ? "OK" : "FAIL";
    cout << res << endl;
    res = (obtener_numero_validar(6543210,5,7)==210) ? "OK" : "FAIL";
    cout << res << endl;
    
    cout << "anclado" << endl;
    anclado(4413444);
    anclado(4402444);
    anclado(4441344,4,5);
    anclado(666666,1,6);
    
    cout << "\n.... end test \n\n" <<endl;
}

int pedirNumero()
{
    int numero;
     do{
         cout << "\nDigite Numero : #";
         cin >> numero;
         numero = validar_tamano_del_numero(numero);
         if(!numero)
         {
             cout << "Error - El numero no es valido" << endl;
         }
        }
     while(numero == 0); 
     return numero;
}

main()
{
      test();
      
      //variables
      int opcion;
      int numero;
      ostringstream mensaje;
      
      do
      {
          cout << "*** NUMERO ANCLADO *** "
               << "\n Escoja un Opcion segun la comprobacion"
               << "\n 1) 3 y 4 d�gitos del n�mero."
               << "\n 2) 2,3 y 4 d�gitos del n�mero."
               << "\n 3) 1 al 6 d�gito del n�mero."
               << "\n 4) Salir."
               << "\n >> ";
          cin >> opcion;
          int inicio;
          int fin;
          
          switch(opcion)
          {
              case 1:
                   inicio = 3;
                   fin = 4;
                   break;
              case 2:
                   inicio = 2;
                   fin = 4;
                   break;
              case 3:
                   inicio = 1;
                   fin = 6;
                   break;
              case 4:
                   continue;                
          }
          //2. verificar primo o no
          if(opcion>=1 && opcion <=3){
          int numero = pedirNumero();
          if( validar_numero_primo_o_no(numero,inicio,fin))
          {
              anclado(numero,inicio,fin);
          }
          }          
      } 
      while(opcion != 4);  
             
}

  
